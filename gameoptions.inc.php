<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * MrJack implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * gameoptions.inc.php
 *
 * MrJack game options description
 * 
 * In this file, you can define your game options (= game variants).
 *   
 * Note: If your game has no variant, you don't have to modify this file.
 *
 * Note²: All options defined in this file should have a corresponding "game state labels"
 *        with the same ID (see "initGameStateLabels" in mrjack.game.php)
 *
 * !! It is not a good idea to modify this file when a game is running !!
 *
 */

$game_options = [

    100 => [
        'name' => totranslate('Lights on board'),    
        'values' => [
                    1 => [ 
                        'name' => totranslate('Lights are visible') 
                    ],

                    2 => [ 
                        'name' => totranslate('No lights'), 
                        'description' => totranslate('No lights showing character visibility will be shown on board.'),
                        'tmdisplay' => totranslate('No lights'),
                        'nobeginner' => true
                    ],            
        ],
        'default' => 1
    ]   
];


