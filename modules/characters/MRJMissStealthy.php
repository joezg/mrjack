<?php

class MRJMissStealthy extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_MISS_STEALTHY; } 

    public function getMovementPoints(){
        return 4;
    }

    public function getabilityTransition() : string
    {
        return "endTurn";
    }

    public function getCanMoveThroughObstacles() : bool 
    { 
        return true; 
    }

    protected function hasBeforeOrAfterAbility() : bool
    {
        return false;
    }

    public static function getName() {
        return "Miss Stealthy";
    }
}