<?php

class MRJSergeantGoodley extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_SERGEANT_GOODLY; } 
    
    protected function hasBeforeOrAfterAbility() : bool
    {
        return true;
    }

    public function getabilityTransition() : string
    {
        return "moveCloser";
    }

    public static function getName() {
        return "Sergeant Goodley";
    }
}