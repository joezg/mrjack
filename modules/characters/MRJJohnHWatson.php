<?php

class MRJJohnHWatson extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_JOHN_H_WATSON; } 

    protected function hasBeforeOrAfterAbility() : bool
    {
        return false;
    }

    public function getabilityTransition() : string
    {
        return "rotateWatson";
    }

    public static function getName() {
        return "John H. Watson";
    }
}