<?php

class MRJJohnSmith extends MRJCharacter{
    public function getId() : string { return MRJCharacter::ID_JOHN_SMITH; } 
    
    protected function hasBeforeOrAfterAbility() : bool
    {
        return true;
    }

    public function getabilityTransition() : string
    {
        return "moveGaslight";
    }

    public static function getName() {
        return "John Smith";
    }
}