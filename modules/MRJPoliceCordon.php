<?php

class MRJPoliceCordon implements \JsonSerializable
{
    private $position;
    private $isActive;
    private $isBeingMoved;

    public function __construct(array $raw) {
        $this->position = $raw["position"];
        $this->isActive = $raw["is_active"] === "1";    
        $this->isBeingMoved = $raw["is_being_moved"] === "1";    
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * Get the value of isActive
     */ 
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set the value of isActive
     *
     * @return  self
     */ 
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get the value of position
     */ 
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the value of position
     *
     * @return  self
     */ 
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get the value of isBeingMoved
     */ 
    public function getIsBeingMoved()
    {
        return $this->isBeingMoved;
    }

    /**
     * Set the value of isBeingMoved
     *
     * @return  self
     */ 
    public function setIsBeingMoved($isBeingMoved)
    {
        $this->isBeingMoved = $isBeingMoved;

        return $this;
    }
}