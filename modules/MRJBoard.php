<?php

class MRJBoard implements \JsonSerializable
{
    private $fields = [];
    private $tokens = [];
    private $pawns = [];
    private $policeCordons = [];

    const EXIT_ID_SW = "sw";
    const EXIT_ID_NW = "nw";
    const EXIT_ID_SE = "se";
    const EXIT_ID_NE = "ne";
    
    public function addField(MRJField $field) : void
    {
        $this->fields[$field->getCoordinatesKey()] = $field;
    }

    public function getFields() : array
    {
        return $this->fields;
    }

    public function addToken(MRJToken $token) : void
    {
        if ($token->getType() === MRJToken::TYPE_PAWN){
            $this->pawns[$token->getCoordinatesKey()] = $token;
            return;
        } 

        $this->tokens[$token->getCoordinatesKey()] = $token;
    }

    public function addTokens(array $tokens) : void
    {
        foreach ($tokens as $token) {
            $this->addToken($token);
        }
    }

    public function addPoliceCordons(array $policeCordons) : void
    {
        $this->policeCordons = $policeCordons;
    }

    public function getCharacterToken($characterId) : MRJToken{
        foreach ($this->pawns as $pawn) {
            /** @var $pawn MRJToken */
            if ($pawn->getId() === $characterId){
                return $pawn;
            }
        }
    }
    
    public function getCharacterField($characterId) : MRJField{
        foreach ($this->pawns as $coordinate => $pawn) {
            /** @var $pawn MRJToken */
            if ($pawn->getId() === $characterId){
                return $this->fields[$coordinate];
            }
        }
    }

    private function isPassableCoordinate($coordinatesKey) : bool
    {
        if (array_key_exists($coordinatesKey, $this->getFields())){
            $field = $this->getFields()[$coordinatesKey];

            return $field->isPassable();
        }

        return false;
    }

    private function isFieldOccupied($coordinatesKey): bool
    {
        return array_key_exists($coordinatesKey, $this->getPawns());
    }

    private function isManholeClosed($coordinatesKey): bool
    {
        if(!array_key_exists($coordinatesKey, $this->getTokens())){
            return false;
        }

        $token = $this->getTokens()[$coordinatesKey];
        
        return $token->getType() == MRJToken::TYPE_MANHOLE;
    }

    private function getAllManholes() : array
    {
        return array_filter($this->getFields(), function($field){
            return $field->getIsManhole();
        });
    }

    public function getDistancesFromField(MRJField $field, $maxDistance = null, $canMoveThroughManholes = true, $includeBlockedFields = false, $includeExits = false): array{
        //the algorithm is variant of dijkstra with this steps:
        // 1. Put starting field marked as unvisited to vertices
        // 2. Pick one unvisited vetrex with lowest distance to visit
        // 3. If distance is equal to maximum distance skip next step
        // 4. Follow all adjacent vertices and update its distance to 
        //    minimum of its distance and current vertex distance + 1. 
        //    (If edge is not in the list, add it.)
        // 5. Mark current vertex as visited.
        // 6. Repeat from 2.

        $vertices = [
            $field->getCoordinatesKey() => [
                "distance" => 0,
                "field" => $field,
                "visited" => false,
                "from" => null,
                "fromDistance" => null,
                "outFromManhole" => false,
                "intoManhole" => false
            ]
        ];

        $getUnvisitedVertices = function($vertices) {
            return array_filter($vertices, function($vertex) { 
                return !$vertex["visited"]; 
            });
        };

        $unvisitedVertices = $getUnvisitedVertices($vertices);

        while (sizeof($unvisitedVertices) > 0){
            uasort($unvisitedVertices, function($a, $b){
                return $a["distance"] <=> $b["distance"];
            });

            //get first element with its key
            foreach(array_slice($unvisitedVertices, 0, 1, true) as $key => $current);

            if (null === $maxDistance || $current["distance"] < $maxDistance) {

                /** @var $currentField MRJField */
                $currentField = $current["field"];
        
                $addNewVertex = function($coordinate, $isFromManhole = false) use (&$vertices, &$current, $includeBlockedFields, $includeExits, $canMoveThroughManholes){
                    if (array_key_exists($coordinate, $this->getFields())){
                        $coordinateField = $this->getFields()[$coordinate];
                        if (array_key_exists($coordinate, $vertices)){
                            if ($current["distance"] + 1 < $vertices[$coordinate]["distance"]){
                                $vertices[$coordinate]["distance"] = $current["distance"] + 1;
                                $vertices[$coordinate]["from"] = $coordinateField->getDirectionFromField($current["field"]);
                                $vertices[$coordinate]["fromDistance"] = $current;
                                $vertices[$coordinate]["outFromManhole"] = $isFromManhole && $canMoveThroughManholes;
                            }
                        } else {
                            if ($this->isPassableCoordinate($coordinate) 
                                || ($includeBlockedFields && !$coordinateField->getIsEdge())
                                || ($includeExits && $coordinateField->getIsExit())
                            ){    
                                $vertices[$coordinate] = [
                                    "distance" => $current["distance"] + 1,
                                    "field" => $coordinateField,
                                    "visited" => false,
                                    "from" => $coordinateField->getDirectionFromField($current["field"]),
                                    "fromDistance" => $current,
                                    "outFromManhole" => $isFromManhole && $canMoveThroughManholes,
                                    "intoManhole" => false,
                                ];
                            }
                        }
                    }
                };
                
                $adjacentCoordinates = $currentField->getAdjacentFieldCoordinates();

                foreach ($adjacentCoordinates as $next) {
                    $addNewVertex($next);
                }

                if ($currentField->getIsManhole() && $canMoveThroughManholes){
                    if (!$this->isManholeClosed($currentField->getCoordinatesKey())){ 
                        $vertices[$key]["intoManhole"] = $maxDistance === null || $current["distance"] < $maxDistance;
                        $current = $vertices[$key];
                        foreach ($this->getAllManholes() as $manhole) {
                            if ($manhole !== $currentField && !$this->isManholeClosed($manhole->getCoordinatesKey())){
                                $addNewVertex($manhole->getCoordinatesKey(), true);
                            }
                        }
                    }
                }

            }

            $vertices[$key]["visited"] = true;
            $unvisitedVertices = $getUnvisitedVertices($vertices);
        }

        return $vertices;
    }

    public function getCharacterMoves(MRJCharacter $character, $includeExits, $includeCaptures){
        $characterToken = $this->getCharacterToken($character->getId());

        if (!array_key_exists($characterToken->getCoordinatesKey(), $this->fields)){
            return [];
        }

        $reachableDistances = $this->getDistancesFromField(
            $this->fields[$characterToken->getCoordinatesKey()], 
            $character->getMovementPoints(), 
            true,
            $character->getCanMoveThroughObstacles(),
            $includeExits
        );

        $fields = [];
        $exits = [];
        $captures = [];
        foreach ($reachableDistances as $key => $distance) {
            if ($distance["distance"] > 0){
                $field = $distance["field"];
                if ($field->getIsExit() && $includeExits){
                    $cordon = $this->policeCordons[$field->getExitTo()];
                    if (!$cordon->getIsActive()){
                        $exits[$key] = $distance;
                    }    
                }
                if ($this->isFieldOccupied($field->getCoordinatesKey())){
                    $pawn = $this->pawns[$field->getCoordinatesKey()];
                    if (!$pawn->getIsFlipped() 
                        && $pawn->getId() != $character->getId()
                        && $includeCaptures){
                            $distance["isCapture"] = true;
                            $captures[$key] = $distance;
                        } 
                } else {
                    if ($this->isPassableCoordinate($field->getCoordinatesKey())){
                        $fields[$key] = $distance;
                    }
                }
            }
        }

        return [
            "fields" => $fields,
            "exits" => $exits,
            "captures" => $captures
        ];
    }

    public function getCharacterMovesTowardsAnother($character, $another, $remainingMovementPoints){
        $anotherToken = $this->getCharacterToken($another->getId());

        if (!array_key_exists($anotherToken->getCoordinatesKey(), $this->fields)){
            return [];
        }

        $distancesFromAnother = $this->getDistancesFromField(
            $this->fields[$anotherToken->getCoordinatesKey()],
            null,
            false
        );

        $characterToken = $this->getCharacterToken($character->getId());
        $distancesFromCharacter = $this->getDistancesFromField(
            $this->fields[$characterToken->getCoordinatesKey()],
            $remainingMovementPoints,
            false
        );

        $distanceBetweenCharacters = $distancesFromAnother[$characterToken->getCoordinatesKey()];

        $possibleDistances = [];

        foreach ($distancesFromCharacter as $key => $distance) {
            if ($distancesFromAnother[$key]["distance"] < $distanceBetweenCharacters["distance"]
                && !$this->isFieldOccupied($distancesFromAnother[$key]["field"]->getCoordinatesKey())
            ){
                $possibleDistances[$key] = $distance;
            }
        }

        return $possibleDistances;
    }

    public function getLights($showLights){
        $lights = [];

        if ($showLights){
            foreach ($this->getPawns() as $key => $pawn) {
                /** @var $field MRJField */
                $field = $this->getFields()[$key];
                $adjacentFields = $field->getAdjacentFieldCoordinates();
                foreach ($adjacentFields as $fieldKey) {
                    if (array_key_exists($fieldKey, $this->getPawns())){
                        $lights[] = [
                            "field" => $this->getFields()[$fieldKey],
                            "type" => "proximity"
                        ];
                    }
                }
            }
    
            foreach ($this->getTokens() as $key => $token) {
                if ($token->getType() == MRJToken::TYPE_GASLIGHT){
                    /** @var $field MRJField */
                    $field = $this->getFields()[$key];
                    $adjacentFields = $field->getAdjacentFieldCoordinates();
    
                    foreach ($adjacentFields as $fieldKey) {
                        if (array_key_exists($fieldKey, $this->getFields())){
                            $lightedField = $this->getFields()[$fieldKey];
                            if (!$lightedField->getIsBlocked()){
                                $lights[] = [
                                    "field" => $lightedField,
                                    "type" => "gaslight",
                                    "rotation" => $field->getDirectionFromField($lightedField)
                                ];
                            }
                        }
                    }                
                }
            }
        }

        $watsonPawn = $this->getCharacterToken(MRJCharacter::ID_JOHN_H_WATSON);
        $watsonField = $this->getCharacterField(MRJCharacter::ID_JOHN_H_WATSON);
        $watsonLightField = $watsonField;
        $length = 0;

        if ($showLights){
            do {
                $watsonLightKey = $watsonLightField->getAdjacentFieldInDirection($watsonPawn->getRotation());
                $watsonLightField = $this->fields[$watsonLightKey];
    
                if ($watsonLightField->isPassable()){
                    $length++;
                }
    
            } while ($watsonLightField->isPassable());
        }
        
        $lights[] = [
            "field" => $watsonField,
            "type" => "watson",
            "rotation" => $watsonPawn->getRotation(),
            "length" => $length
        ];

        return $lights;
    }

    public function getCharacterVisibilities(){

        $visibleCoordinates = [];
        foreach ($this->getPawns() as $key => $pawn) {
            /** @var $field MRJField */
            $field = $this->getFields()[$key];
            $adjacentFields = $field->getAdjacentFieldCoordinates();
            $visibleCoordinates = array_merge($visibleCoordinates, $adjacentFields);
        }

        foreach ($this->getTokens() as $key => $token) {
            if ($token->getType() == MRJToken::TYPE_GASLIGHT){
                /** @var $field MRJField */
                $field = $this->getFields()[$key];
                $adjacentFields = $field->getAdjacentFieldCoordinates();
                $visibleCoordinates = array_merge($visibleCoordinates, $adjacentFields);
            }
        }

        $watsonPawn = $this->getCharacterToken(MRJCharacter::ID_JOHN_H_WATSON);
        $watsonLightField = $this->getCharacterField(MRJCharacter::ID_JOHN_H_WATSON);
        do {
            $watsonLightKey = $watsonLightField->getAdjacentFieldInDirection($watsonPawn->getRotation());
            $watsonLightField = $this->fields[$watsonLightKey];

            if ($watsonLightField->isPassable()){
                $visibleCoordinates[] = $watsonLightKey;
            }

        } while ($watsonLightField->isPassable());

        $visibleCharacters = [];
        $invisibleCharacters = [];
        foreach ($this->getPawns() as $key => $pawn) {
            if (array_search($key, $visibleCoordinates) !== false){
                $visibleCharacters[$pawn->getId()] = $pawn;
            } else {
                $invisibleCharacters[$pawn->getId()] = $pawn;
            }
        }

        return [
            "visible" => $visibleCharacters, 
            "invisible" => $invisibleCharacters
        ];

    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * Get the value of tokens
     */ 
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * Get the value of pawns
     */ 
    public function getPawns()
    {
        return $this->pawns;
    }
}