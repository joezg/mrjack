<?php

class MRJField implements \JsonSerializable
{
    private $x;
    private $y;
    private $isExit = false;
    private $exitTo = null;
    private $isGaslight = false;
    private $isManhole = false;
    private $isBlocked = false;
    private $isEdge = false;

    public function __construct(int $x, int $y) 
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function isPassable() : bool
    {
        return !$this->isGaslight && !$this->isBlocked;
    }

    public function getX() : int 
    {
        return $this->x;
    }
    
    public function getY() : int
    {
        return $this->y;
    }
    
    public function getZ() : int
    {
        return 0 - $this->x - $this->y;
    }

    public function setX(int $x) : void
    {
        $this->x = $x;
    }
    
    public function setY(int $y) : void
    {
        $this->y = $y;
    }

    public function getCoordinatesKey() : string
    {
        return $this->x.$this->y;
    }

    public function setIsExit(bool $isExit) : void
    {
        $this->isExit = $isExit;
    }
    
    public function setIsEdge(bool $isEdge) : void
    {
        $this->isEdge = $isEdge;
    }

    public function setIsGaslight(bool $isGaslight) : void 
    {
        $this->isGaslight = $isGaslight;
    }
    
    public function setIsManhole(bool $isManhole) : void 
    {
        $this->isManhole = $isManhole;
    }

    public function getRotationFromAdjacentCoordinates($x, $y){
        if ($x > $this->x){
            if ($y > $this->y){
                return MRJToken::ROTATION_NW;
            }

            return MRJToken::ROTATION_SW;
        }

        if ($x < $this->x){
            if ($y < $this->y){
                return MRJToken::ROTATION_SE;
            }

            return MRJToken::ROTATION_NE;
        }

        if ($y > $this->y){
            return MRJToken::ROTATION_N;
        }

        return MRJToken::ROTATION_S;
    }

    public function getDirectionFromField(MRJField $source){
        if ($source->getX() === $this->getX()){
            if ($source->getY() > $this->getY()){
                return MRJToken::ROTATION_N;
            }
            if ($source->getY() < $this->getY()){
                return MRJToken::ROTATION_S;
            }
        }
        
        if ($source->getX() > $this->getX()){
            if ($source->getY() > $this->getY()){
                return MRJToken::ROTATION_NW;
            }
            if ($source->getY() === $this->getY()){
                return MRJToken::ROTATION_SW;
            }
        }
        if ($source->getX() < $this->getX()){
            if ($source->getY() === $this->getY()){
                return MRJToken::ROTATION_NE;
            }
            if ($source->getY() < $this->getY()){
                return MRJToken::ROTATION_SE;
            }
        }

        return "";
    }

    public function getAdjacentFieldInDirection($rotation){
        switch ($rotation) {
            case MRJToken::ROTATION_N:
                return $this->x.($this->y + 1);
            
            case MRJToken::ROTATION_S:
                return $this->x.($this->y - 1);
            
            case MRJToken::ROTATION_NE:
                return ($this->x - 1).$this->y;
            
            case MRJToken::ROTATION_NW:
                return ($this->x + 1).($this->y + 1);
            
            case MRJToken::ROTATION_SW:
                return ($this->x + 1).$this->y;
            
            case MRJToken::ROTATION_SE:
                return ($this->x - 1).($this->y - 1);
            
            default:
                return "";
        }
    }

    public function getAdjacentFieldCoordinates() : array
    {
        $coordinatesKeys = [];

        $coordinatesKeys[] = $this->x.($this->y + 1);
        $coordinatesKeys[] = $this->x.($this->y - 1);
        $coordinatesKeys[] = ($this->x - 1).$this->y;
        $coordinatesKeys[] = ($this->x + 1).$this->y;
        $coordinatesKeys[] = ($this->x + 1).($this->y + 1);
        $coordinatesKeys[] = ($this->x - 1).($this->y - 1);

        return $coordinatesKeys;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }


    /**
     * Get the value of isExit
     */ 
    public function getIsExit()
    {
        return $this->isExit;
    }

    /**
     * Get the value of isEdge
     */ 
    public function getIsEdge()
    {
        return $this->isEdge;
    }

    /**
     * Get the value of isGaslight
     */ 
    public function getIsGaslight()
    {
        return $this->isGaslight;
    }

    /**
     * Get the value of isManhole
     */ 
    public function getIsManhole()
    {
        return $this->isManhole;
    }

    /**
     * Get the value of exitTo
     */ 
    public function getExitTo()
    {
        return $this->exitTo;
    }

    /**
     * Set the value of exitTo
     *
     * @return  self
     */ 
    public function setExitTo($exitTo)
    {
        $this->exitTo = $exitTo;

        return $this;
    }

    /**
     * Get the value of isBlocked
     */ 
    public function getIsBlocked()
    {
        return $this->isBlocked;
    }

    /**
     * Set the value of isBlocked
     *
     * @return  self
     */ 
    public function setIsBlocked($isBlocked)
    {
        $this->isBlocked = $isBlocked;

        return $this;
    }
}