{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- MrJack implementation : © Jurica Hladek (jurica.hladek@gmail.com)
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------
-->
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Special+Elite&display=swap" rel="stylesheet">

<div id="play-area">

    <div id="card-display">
        <div id="character-card-0" class="character-card"></div>
        <div id="character-card-1" class="character-card"></div>
        <div id="character-card-2" class="character-card"></div>
        <div id="character-card-3" class="character-card"></div>
    </div>
    
    <div class="info-wrapper">
        <div class="round-wrapper">
            <div id="round-number"></div>
        </div>
        <div class="witness-card-wrapper">
            <div id="witness-card" class="witness-card">
                <div class="witness-card-face back"></div>
                <div class="witness-card-face front"></div>
            </div>
        </div>
        <div id="toggle-lights-wrapper">
            <div id="toggle-lights" class="lights-on"></div>
        </div>
        <div class="deck-wrapper">
            <div id="character-card-deck" class="facedown-deck"></div>
            <div class="card-number"><span id="character-card-number"></span>x</div>
        </div>
        <div class="deck-wrapper">
            <div id="alibi-card-deck" class="facedown-deck"></div>
            <div class="card-number"><span id="alibi-card-number"></span>x</div>
        </div>
    </div>

    <div id="board-wrapper">
        <div id="responsive-board">
            <div id="board">
                <div id="house-lights">
                    <div id="house-light-1" class="glow-1"></div>
                    <div id="house-light-2" class="glow-2""></div>
                    <div id="house-light-3" class="glow-1""></div>
                    <div id="house-light-4" class="glow-1""></div>
                    <div id="house-light-5" class="glow-3""></div>
                    <div id="house-light-6" class="glow-2""></div>
                    <div id="house-light-7" class="glow-1""></div>
                    <div id="house-light-8" class="glow-3""></div>
                    <div id="house-light-9" class="glow-2""></div>
                    <div id="house-light-10" class="glow-3""></div>
                    <div id="house-light-11" class="glow-2""></div>
                </div>
                <!-- BEGIN field -->
                <div id="field-{X}-{Y}" class="field {BLOCKED_CLASS} z-order-{Z_ORDER}" style="top: {TOP}%; left: {LEFT}%;">
                    <div id="field-intreaction-{X}-{Y}" class="field-interaction" data-x={X} data-y={Y}></div>
                    <div class="field-visuals"></div>
                    <div class="token" id="token-{X}-{Y}"></div>
                    <div class="pawn" id="pawn-{X}-{Y}">
                        <div class="pawn-visual">
                            <div class="pawn-visual-face back"></div>
                            <div class="pawn-visual-face front"></div>
                        </div>
                        <div class="pawn-effect"></div>
                    </div>
                    <div class="lights-wrapper" id="lights-{X}-{Y}">
                    </div>
                </div>
                <!-- END field -->

                <div id="police-cordon-nw" class="police-cordon" data-position="nw"></div>
                <div id="police-cordon-sw" class="police-cordon" data-position="sw"></div>
                <div id="police-cordon-ne" class="police-cordon" data-position="ne"></div>
                <div id="police-cordon-se" class="police-cordon" data-position="se"></div>
            </div>
        </div>
        <div id="character-ability"></div>
    </div>
</div>

<audio id="audiosrc_o_mrjack_moveCordon" src="{GAMETHEMEURL}img/cordon_move.ogg" preload="none" autobuffer></audio>
<audio id="audiosrc_mrjack_moveCordon" src="{GAMETHEMEURL}img/cordon_move.mp3" preload="none" autobuffer></audio>
<audio id="audiosrc_o_mrjack_lightsOff" src="{GAMETHEMEURL}img/lights_off.ogg" preload="none" autobuffer></audio>
<audio id="audiosrc_mrjack_lightsOff" src="{GAMETHEMEURL}img/lights_off.mp3" preload="none" autobuffer></audio>
<audio id="audiosrc_o_mrjack_lightsOn" src="{GAMETHEMEURL}img/lights_on.ogg" preload="none" autobuffer></audio>
<audio id="audiosrc_mrjack_lightsOn" src="{GAMETHEMEURL}img/lights_on.mp3" preload="none" autobuffer></audio>
<audio id="audiosrc_o_mrjack_moveManhole" src="{GAMETHEMEURL}img/manhole_move.ogg" preload="none" autobuffer></audio>
<audio id="audiosrc_mrjack_moveManhole" src="{GAMETHEMEURL}img/manhole_move.mp3" preload="none" autobuffer></audio>
<audio id="audiosrc_o_mrjack_punch" src="{GAMETHEMEURL}img/punch.ogg" preload="none" autobuffer></audio>
<audio id="audiosrc_mrjack_punch" src="{GAMETHEMEURL}img/punch.mp3" preload="none" autobuffer></audio>
<audio id="audiosrc_o_mrjack_gaslightSound" src="{GAMETHEMEURL}img/watson_light.ogg" preload="none" autobuffer></audio>
<audio id="audiosrc_mrjack_gaslightSound" src="{GAMETHEMEURL}img/watson_light.mp3" preload="none" autobuffer></audio>

<script type="text/javascript">
// Javascript HTML templates

var jstpl_player_board = '\
<div>\
    <span id="player-role-${id}"></span>\
    <div class="character-and-alibi-cards">\
        <div id="mrjack-character-${id}" class="character-appearance">\
        </div>\
        <div id="private-cards-${id}" class="private-cards">\
        </div>\
    </div>\
</div>';

var jstpl_character_ability = '\
<div class="character-ability-box ${id}">\
    <div class="character-name">\
        <div class="name-wrapper">\
            <span class="name-type">${name}</span>\
        </div>\
        <div class="character-status ${characterStatusClass}">${characterStatus}</div>\
        <div class="more-info">{MORE_INFO}</div>\
    </div>\
    <div class="character-info">\
        <div class="character-text">\
            <div class="character-ability">\
                <h3>${specialAbilityHeader} (${isMandatory}):</h3>\
                ${ability}\
            </div>\
            <div class="character-movement-points">Movement points: <span>${maxMovement}</span></div>\
        </div>\
        <div class="character-appearance ${id}"></div>\
    </div>\
    <div class="character-hints">\
        <p class="hint-cancel"><small>{CANCEL_HELP}</small></p>\
        <p class="hint-select"><small>{SELECT_HELP}</small></p>\
        <p class="hint-hint"><small>{HINT_HELP}</small></p>\
    <\div>\
</div>';

var jstpl_light = '\
    <div class="lights lights-${type}">\
        <div class="lights-transform">\
        </div>\
    </div>';

var jstpl_roundTooltip = '\
    <div class="round-tooltip">\
        <div class="round-number">\
            <h2>Round ${round}/8</h2>\
        </div>\
        <div class="turn-order">\
            <h2>Turn order</h2>\
            ${turnOrder}\
        </div>\
        <div class="round-end">\
            <h2>Round end</h2>\
            ${roundEnd}\
        </div>\
    </div>';

var jstpl_endDialogContent = '\
    <div class="end-game-dialog">\
        <div class="content">\
            <div class="message">\
                ${message}\
                <div class="mr-jack-character">\
                    {MR_JACK_WAS}<span class="mr-jack-character-name">${characterName}</span>\
                </div>\
            </div>\
            <div class="character-appearance ${id}"></div>\
        </div>\
        <a class="bgabutton bgabutton_blue confirm-button">OK</a>\
    </div>';

</script>  

{OVERALL_GAME_FOOTER}
