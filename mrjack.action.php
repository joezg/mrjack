<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * MrJack implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on https://boardgamearena.com.
 * See http://en.doc.boardgamearena.com/Studio for more information.
 * -----
 * 
 * mrjack.action.php
 *
 * MrJack main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *       
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall( "/mrjack/mrjack/myAction.html", ...)
 *
 */
  
  
  class action_mrjack extends APP_GameAction
  { 
    // Constructor: please do not modify
   	public function __default()
  	{
  	    if( $this->isArg( 'notifwindow') )
  	    {
            $this->view = "common_notifwindow";
  	        $this->viewArgs['table'] = $this->getArg( "table", AT_int, true );
  	    }
  	    else
  	    {
            $this->view = "mrjack_mrjack";
            $this->trace( "Complete reinitialization of board game" );
      }
  	} 
  	
  	public function selectCharacterCard()
    {
        $this->setAjaxMode();     
        $cardType = $this->getArg( "type", AT_alphanum, true );
        $this->game->selectCharacterCard( $cardType );
        $this->ajaxResponse( );
    }
    
    public function selectCharacterByField()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->selectCharacterByField( $x, $y );
        $this->ajaxResponse( );
    }

    public function selectSourceManhole()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->selectSourceManhole( $x, $y );
        $this->ajaxResponse( );
    }
    
    public function selectManholeDestination()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->selectManholeDestination( $x, $y );
        $this->ajaxResponse( );
    }
    
    public function selectSourceGaslight()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->selectSourceGaslight( $x, $y );
        $this->ajaxResponse( );
    }
    
    public function selectGaslightDestination()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->selectGaslightDestination( $x, $y );
        $this->ajaxResponse( );
    }
    
    public function moveCharacter()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->moveCharacter( $x, $y );
        $this->ajaxResponse( );
    }
    
    public function characterExit()
    {
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->characterExit($x, $y);
        $this->ajaxResponse( );
    }
    
    public function selectSourceCordon()
    {
        $this->setAjaxMode();     
        $position = $this->getArg( "position", AT_alphanum, true );
        $this->game->selectSourceCordon( $position );
        $this->ajaxResponse( );
    }
    
    public function selectCordonDestination()
    {
        $this->setAjaxMode();     
        $position = $this->getArg( "position", AT_alphanum, true );
        $this->game->selectCordonDestination( $position );
        $this->ajaxResponse( );
    }

    public function characterCapture(){
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->characterCapture( $x, $y );
        $this->ajaxResponse( );
    }
    
    public function decideAbility(){
        $this->setAjaxMode();     
        $this->game->decideAbility();
        $this->ajaxResponse( );
    }
    
    public function cancelCharacter(){
        $this->setAjaxMode();     
        $this->game->cancelCharacter();
        $this->ajaxResponse( );
    }
    
    public function cancelSelection(){
        $this->setAjaxMode();     
        $this->game->cancelSelection();
        $this->ajaxResponse( );
    }
    
    public function cancelAbility(){
        $this->setAjaxMode();     
        $this->game->cancelAbility();
        $this->ajaxResponse( );
    }
    
    public function switchPlace(){
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->switchPlace($x, $y);
        $this->ajaxResponse( );
    }
    
    public function pickCharacterToMoveCloser(){
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->pickCharacterToMoveCloser($x, $y);
        $this->ajaxResponse( );
    }
    
    public function moveCloser(){
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->moveCloser($x, $y);
        $this->ajaxResponse( );
    }
    
    public function rotateWatson(){
        $this->setAjaxMode();     
        $x = $this->getArg( "x", AT_int, true );
        $y = $this->getArg( "y", AT_int, true );
        $this->game->rotateWatson($x, $y);
        $this->ajaxResponse( );
    }
    
    public function confirmWatsonRotation(){
        $this->setAjaxMode();     
        $this->game->confirmWatsonRotation();
        $this->ajaxResponse( );
    }

    /*
    
    Example:
  	
    public function myAction()
    {
        $this->setAjaxMode();     

        // Retrieve arguments
        // Note: these arguments correspond to what has been sent through the javascript "ajaxcall" method
        $arg1 = $this->getArg( "myArgument1", AT_posint, true );
        $arg2 = $this->getArg( "myArgument2", AT_posint, true );

        // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
        $this->game->myAction( $arg1, $arg2 );

        $this->ajaxResponse( );
    }
    
    */

  }
  

