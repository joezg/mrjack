<?php 

include "C:\Users\juric\projects\bga\mrjack\modules\MRJBoard.php";
include "C:\Users\juric\projects\bga\mrjack\modules\MRJBoardFactory.php";
include "C:\Users\juric\projects\bga\mrjack\modules\MRJCharacter.php";
include "C:\Users\juric\projects\bga\mrjack\modules\MRJField.php";
include "C:\Users\juric\projects\bga\mrjack\modules\MRJToken.php";
include "C:\Users\juric\projects\bga\mrjack\modules\MRJPoliceCordon.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJSherlockHolmes.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJJohnHWatson.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJJohnSmith.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJInspectorLestrade.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJMissStealthy.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJSergeantGoodley.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJSirWilliamGull.php";
include "C:\Users\juric\projects\bga\mrjack\modules\characters/MRJJeremyBert.php";

$board = MRJBoardFactory::getBoard();

$raw = [
    [
      "type" => "gaslight",
      "id" => "4",
      "x" => "2",
      "y" => "1",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "gaslight",
      "id" => "2",
      "x" => "1",
      "y" => "5",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "gaslight",
      "id" => "5",
      "x" => "5",
      "y" => "6",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "gaslight",
      "id" => "6",
      "x" => "5",
      "y" => "2",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "gaslight",
      "id" => "3",
      "x" => "10",
      "y" => "10",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "sg",
      "x" => "6",
      "y" => "5",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "jb",
      "x" => "9",
      "y" => "9",
      "is_flipped" => true,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "jhw",
      "x" => "11",
      "y" => "5",
      "is_flipped" => true,
      "is_being_moved" => false,
      "rotation" => "n",
    ],
    [
      "type" => "pawn",
      "id" => "sh",
      "x" => "8",
      "y" => "4",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "js",
      "x" => "6",
      "y" => "9",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "il",
      "x" => "11",
      "y" => "7",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "swg",
      "x" => "2",
      "y" => "4",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "pawn",
      "id" => "ms",
      "x" => "8",
      "y" => "5",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "manhole",
      "id" => "1",
      "x" => "1",
      "y" => "6",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ],
    [
      "type" => "manhole",
      "id" => "2",
      "x" => "10",
      "y" => "7",
      "is_flipped" => false,
      "is_being_moved" => false,
      "rotation" => "",
    ]
];

$tokens = array_map(function($raw){
    return new MRJToken($raw);
}, $raw);

$board->addTokens($tokens);

var_dump($board->getCharacterVisibilities());