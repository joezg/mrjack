<?php
 /**
  *------
  * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
  * MrJack implementation : © <Your name here> <Your email address here>
  * 
  * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
  * See http://en.boardgamearena.com/#!doc/Studio for more information.
  * -----
  * 
  * mrjack.game.php
  *
  * This is the main file for your game logic.
  *
  * In this PHP file, you are going to defines the rules of the game.
  *
  */


require_once( APP_GAMEMODULE_PATH.'module/table/table.game.php' );
require_once('modules/MRJBoard.php');
require_once('modules/MRJBoardFactory.php');
require_once('modules/MRJField.php');
require_once('modules/MRJToken.php');
require_once('modules/MRJPoliceCordon.php');
require_once('modules/MRJCharacter.php');
require_once('modules/characters/MRJSherlockHolmes.php');
require_once('modules/characters/MRJJohnHWatson.php');
require_once('modules/characters/MRJJohnSmith.php');
require_once('modules/characters/MRJInspectorLestrade.php');
require_once('modules/characters/MRJMissStealthy.php');
require_once('modules/characters/MRJSergeantGoodley.php');
require_once('modules/characters/MRJSirWilliamGull.php');
require_once('modules/characters/MRJJeremyBert.php');

class MrJack extends Table
{
    private const GLOBAL_MR_JACK_PLAYER_ID = 'mr_jack_player_id';
    private const GLOBAL_MR_JACK_WAS_VISIBLE = 'mr_jack_was_visible';
    private const GLOBAL_ROUND_NUMBER = 'round-number';
    private const GLOBAL_SPECIAL_ACTION_USED = 'special-action-used';
    private const GLOBAL_SG_MOVEMENT_POINTS_LEFT = 'sg-movement-points-left';
    private const GLOBAL_MOVE_USED = "move_used";
    private const GLOBAL_MR_JACK_KNOWN = "mr_jack_known";
    private const OPTION_NO_LIGHTS = "no_lights";
    private const CARD_LOCATION_DECK = "deck";
    private const CARD_LOCATION_HAND = "hand";
    private const CARD_LOCATION_DISPLAY = "display";
    private const CARD_LOCATION_DISCARD = "discard";
    private const CARD_LOCATION_MRJACK = "mrjack";
    private const MODULE_DECK = "module.common.deck";
    private const CARD_TYPE_ALIBI = "alibi_card";
    private const CARD_TYPE_CHARACTER = "character_card";
    private const CARD_DISPLAY_STATUS_READY = 0;
    private const CARD_DISPLAY_STATUS_SELECTED = 1;
    private const CARD_DISPLAY_STATUS_USED = 2;
    private const STAT_ROUND_NUMBER = "round_number";
    private const STAT_MRJACK_WON = "mrjack_won";
    private const STAT_DETECTIVE_WON = "detective_won";
    private const STAT_END_BY_CAPTURE = "end_by_capture";
    private const STAT_CAPTURE_ATTEMPT_SUCCESSFUL = "capture_attempt_successful";
    private const STAT_END_BY_ESCAPE = "end_by_escape";
    private const STAT_END_BY_TIME = "end_by_time";
    private const STAT_WON = "won";
    private const STAT_PLAYED_AS_MRJACK = "played_as_mrjack";
    private const STAT_PLAYED_AS_DETECTIVE = "played_as_detective";
    private const STAT_WON_AS_MRJACK = "won_as_mrjack";
    private const STAT_WON_AS_DETECTIVE = "won_as_detective";
    private const STAT_LOST_AS_MRJACK = "lost_as_mrjack";
    private const STAT_LOST_AS_DETECTIVE = "lost_as_detective";
    private const STAT_WON_BY_CAPTURE = "won_by_capture";
    private const STAT_LOST_BY_CAPTURE = "lost_by_capture";
    private const STAT_WON_BY_INCORRECT_CAPTURE = "won_by_incorrect_capture";
    private const STAT_LOST_BY_INCORRECT_CAPTURE = "lost_by_incorrect_capture";
    private const STAT_WON_BY_ESCAPE = "won_by_escape";
    private const STAT_LOST_BY_ESCAPE = "lost_by_escape";
    private const STAT_WON_BY_TIME = "won_by_time";
    private const STAT_LOST_BY_TIME = "lost_by_time";
    private const STAT_PICKED_MS = "picked_ms";
    private const STAT_PICKED_SH = "picked_sh";
    private const STAT_PICKED_JHW = "picked_jhw";
    private const STAT_PICKED_SWG = "picked_swg";
    private const STAT_SWG_MOVED = "swg_moved";
    private const STAT_SWG_SWITCH = "swg_switch";
    private const STAT_PICKED_SG = "picked_sg";
    private const STAT_PICKED_JS = "picked_js";
    private const STAT_PICKED_JB = "picked_jb";
    private const STAT_PICKED_IL = "picked_il";
    private const STAT_MRJACK_VISIBLE_PERCENTAGE = "mrjack_visible_percentage";
    private const STAT_INNOCENT_AFTER_1 = "innocent_after_1";
    private const STAT_INNOCENT_AFTER_2 = "innocent_after_2";
    private const STAT_INNOCENT_AFTER_3 = "innocent_after_3";
    private const STAT_INNOCENT_AFTER_4 = "innocent_after_4";
    private const STAT_INNOCENT_AFTER_5 = "innocent_after_5";
    private const STAT_INNOCENT_AFTER_6 = "innocent_after_6";
    private const STAT_INNOCENT_AFTER_7 = "innocent_after_7";
    
    private $alibiCards;
    private $characterCards;

	function __construct( )
	{
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();
        
        $this->alibiCards = self::getNew( self::MODULE_DECK );
        $this->alibiCards->init( self::CARD_TYPE_ALIBI );
        
        $this->characterCards = self::getNew( self::MODULE_DECK );
        $this->characterCards->init( self::CARD_TYPE_CHARACTER );
        $this->characterCards->autoreshuffle = true;
        
        $this->initGameStateLabels([ 
            self::GLOBAL_MR_JACK_PLAYER_ID => 10,
            self::GLOBAL_MR_JACK_WAS_VISIBLE => 11,
            self::GLOBAL_ROUND_NUMBER => 12,
            self::GLOBAL_SPECIAL_ACTION_USED => 13,
            self::GLOBAL_SG_MOVEMENT_POINTS_LEFT => 14,
            self::GLOBAL_MOVE_USED => 15,
            self::GLOBAL_MR_JACK_KNOWN => 16,
            self::OPTION_NO_LIGHTS => 100,
        ]);        
	}
	
    protected function getGameName( )
    {
		// Used for translations and stuff. Please do not modify.
        return "mrjack";
    }	

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame( $players, $options = array() )
    {    
        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $gameinfos = $this->getGameinfos();
        $default_colors = $gameinfos['player_colors'];
 
        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        foreach( $players as $player_id => $player )
        {
            $color = array_shift( $default_colors );
            $values[] = "('".$player_id."','$color','".$player['player_canal']."','".addslashes( $player['player_name'] )."','".addslashes( $player['player_avatar'] )."')";
        }
        $sql .= implode( ',', $values );
        $this->DbQuery( $sql );
        $this->reattributeColorsBasedOnPreferences( $players, $gameinfos['player_colors'] );
        $this->reloadPlayersBasicInfos();
        
        /************ Start the game initialization *****/
        
        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        $this->initStat("table", self::STAT_ROUND_NUMBER, 0);
        $this->initStat("table", self::STAT_MRJACK_WON, 0);
        $this->initStat("table", self::STAT_DETECTIVE_WON, 0);
        $this->initStat("table", self::STAT_END_BY_CAPTURE, 0);
        $this->initStat("table", self::STAT_CAPTURE_ATTEMPT_SUCCESSFUL, 0);
        $this->initStat("table", self::STAT_END_BY_ESCAPE, 0);
        $this->initStat("table", self::STAT_END_BY_TIME, 0);
        $this->initStat("player", self::STAT_ROUND_NUMBER, 0);
        $this->initStat("player", self::STAT_WON, 0);
        $this->initStat("player", self::STAT_PLAYED_AS_MRJACK, 0);
        $this->initStat("player", self::STAT_PLAYED_AS_DETECTIVE, 0);
        $this->initStat("player", self::STAT_WON_AS_MRJACK, 0);
        $this->initStat("player", self::STAT_WON_AS_DETECTIVE, 0);
        $this->initStat("player", self::STAT_LOST_AS_MRJACK, 0);
        $this->initStat("player", self::STAT_LOST_AS_DETECTIVE, 0);
        $this->initStat("player", self::STAT_WON_BY_CAPTURE, 0);
        $this->initStat("player", self::STAT_LOST_BY_CAPTURE, 0);
        $this->initStat("player", self::STAT_WON_BY_INCORRECT_CAPTURE, 0);
        $this->initStat("player", self::STAT_LOST_BY_INCORRECT_CAPTURE, 0);
        $this->initStat("player", self::STAT_WON_BY_ESCAPE, 0);
        $this->initStat("player", self::STAT_LOST_BY_ESCAPE, 0);
        $this->initStat("player", self::STAT_WON_BY_TIME, 0);
        $this->initStat("player", self::STAT_LOST_BY_TIME, 0);
        $this->initStat("player", self::STAT_PICKED_MS, 0);
        $this->initStat("player", self::STAT_PICKED_SH, 0);
        $this->initStat("player", self::STAT_PICKED_JHW, 0);
        $this->initStat("player", self::STAT_PICKED_SWG, 0);
        $this->initStat("player", self::STAT_SWG_MOVED, 0);
        $this->initStat("player", self::STAT_SWG_SWITCH, 0);
        $this->initStat("player", self::STAT_PICKED_SG, 0);
        $this->initStat("player", self::STAT_PICKED_JS, 0);
        $this->initStat("player", self::STAT_PICKED_JB, 0);
        $this->initStat("player", self::STAT_PICKED_IL, 0);
        $this->initStat("player", self::STAT_MRJACK_VISIBLE_PERCENTAGE, 100);
        
        $this->setupBoardAndCards();
        $this->setGameStateInitialValue(self::GLOBAL_MR_JACK_WAS_VISIBLE, 1);
        $this->setGameStateInitialValue(self::GLOBAL_ROUND_NUMBER, 0);
        $this->setGameStateInitialValue(self::GLOBAL_SPECIAL_ACTION_USED, 0);
        $this->setGameStateInitialValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT, 0);
        $this->setGameStateInitialValue(self::GLOBAL_MOVE_USED, 0);

        // Activate first player (which is in general a good idea :) )
        $this->activeNextPlayer();

        $activePlayerId = $this->getActivePlayerId();
        $mrJackPlayerId = $this->getPlayerAfter($activePlayerId); 

        $this->setStat(1, self::STAT_PLAYED_AS_MRJACK, $mrJackPlayerId);
        $this->setStat(1, self::STAT_PLAYED_AS_DETECTIVE, $activePlayerId);
        
        $this->setGameStateInitialValue(self::GLOBAL_MR_JACK_PLAYER_ID, $mrJackPlayerId);
        $this->setGameStateInitialValue(self::GLOBAL_MR_JACK_KNOWN, 0);
        $this->alibiCards->pickCardForLocation(self::CARD_LOCATION_DECK, self::CARD_LOCATION_MRJACK, $mrJackPlayerId );

        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array();
    
        $currentPlayerId = $this->getCurrentPlayerId();    // !! We must only return informations visible by this player !!
    
        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score FROM player ";
        $players = $this->getCollectionFromDb( $sql );
        $result['players'] = $players;
  
        $result["tokens"] = json_encode($this->getTokens());
        $result["policeCordons"] = json_encode($this->getPoliceCordons());
        $result["remainingAlibiCardsNumber"] = $this->alibiCards->countCardInLocation(self::CARD_LOCATION_DECK);
        $result["remainingCharacterCardsNumber"] = $this->characterCards->countCardInLocation(self::CARD_LOCATION_DECK);
        
        $result["privateAlibiCards"] = [];

        foreach ($players as $player) {
            if ($player["id"] == $currentPlayerId){
                $result["privateAlibiCards"][$player["id"]] = $this->alibiCards->getCardsInLocation(self::CARD_LOCATION_HAND, $player["id"]);
            } else {
                $result["privateAlibiCards"][$player["id"]] = $this->alibiCards->countCardInLocation(self::CARD_LOCATION_HAND, $player["id"]);
            }
        }

        $result["characterCardsDisplay"] = $this->getDisplayCardsWithCharacterStatuses();

        $result["mrJackPlayerId"] = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID); 
        $isMrJack = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID) == $currentPlayerId;
        $isMrJackKnown = $this->getGameStateValue(self::GLOBAL_MR_JACK_KNOWN) == 1;
        $result["isMrJack"] = $isMrJack;

        $result["mrJackCharacterId"] = null;
        if ($isMrJack || $isMrJackKnown){
            $result["mrJackCharacterId"] = $this->getMrJackCharacter();
        }
        
        $result["mrJackWasVisible"] = $this->getGameStateValue(self::GLOBAL_MR_JACK_WAS_VISIBLE) == 1;
        $result["roundNumber"] = $this->getGameStateValue(self::GLOBAL_ROUND_NUMBER);

        $result["lights"] = $this->getLights();
        $result["noLightsOption"] = $this->getGameStateValue(self::OPTION_NO_LIGHTS) == 2;
        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        $round = $this->getGameStateValue(self::GLOBAL_ROUND_NUMBER);
        $playedCards = $this->characterCards->countCardInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_USED);

        $totalCardsPlayed = ($round -1) * 4 + $playedCards;

        return round($totalCardsPlayed/ 8 / 4 * 100);
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Utility functions
////////////    

    /*
        In this space, you can put any utility methods useful for your game logic
    */
    public function array_find($array, $predicate){
        foreach ($array as $value) {
            if ($predicate($value) === true){
                return $value;
            }
        }

        return null;
    }

    function getDisplayCardsWithCharacterStatuses(){
        $characterCards = $this->characterCards->getCardsInLocation(self::CARD_LOCATION_DISPLAY);
        $tokens = $this->getTokens();

        foreach ($characterCards as $id => $card) {
            $currentToken = $this->array_find($tokens, function($token) use ($card){
                return $token->getId() == $card["type"];
            });
            $characterCards[$id]["isInnocent"] = $currentToken->getIsFlipped();
        }

        return $characterCards;
    }

    public function getSelectedCharacter() : ?MRJCharacter
    {
        $selectedCards = $this->characterCards->getCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_SELECTED);
        $characterId = reset($selectedCards)["type"];

        if ($characterId === null){
            return null;
        }

        return MRJCharacter::getCharacter($characterId);
    }

    public function checkIfAbilityDone() : bool{
        return $this->getGameStateValue(self::GLOBAL_SPECIAL_ACTION_USED) == 1;
    }
    
    public function checkIfMoveDone() : bool{
        return $this->getGameStateValue(self::GLOBAL_MOVE_USED) == 1;
    }

    public function getMrJackCharacter(){
        $mrJackCardArray = $this->alibiCards->getCardsInLocation(self::CARD_LOCATION_MRJACK);
        //reset returns the first element of array
        return reset($mrJackCardArray)["type"];
    }

    public function getBoardFields(){
        return MRJBoardFactory::getBoard()->getFields();
    }

    public function getBoardWithTokens() : MRJBoard 
    {
        $board = MRJBoardFactory::getBoard();
        $board->addTokens($this->getTokens());
        
        return $board;
    }

    public function getPossibleMoves(MRJCharacter $character){
        $board = $this->getBoardWithTokens();
        $board->addPoliceCordons($this->getPoliceCordons());

        $includeExits = $this->getMrJackCharacter() == $character->getId() 
            && $this->getGameStateValue(self::GLOBAL_MR_JACK_WAS_VISIBLE) == 0
            && $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID) == $this->getCurrentPlayerId();

        $includeCaptures = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID) != $this->getCurrentPlayerId();

        return $board->getCharacterMoves($character, $includeExits, $includeCaptures);
    }

    public function getCharacterVisibilities(){
        $board = MRJBoardFactory::getBoard();
        $board->addTokens($this->getTokens());

        return $board->getCharacterVisibilities();
    }

    public function getTokens(){
        $sql = "SELECT type, id, x, y, is_flipped, is_being_moved, rotation FROM token ";
        return array_map(function($token){
            return new MRJToken($token);
        }, $this->getObjectListFromDB($sql));
    }

    public function getTokensOfKind($type){
        $sql = "SELECT type, id, x, y, is_flipped, is_being_moved, rotation FROM token WHERE type = '$type'";

        $tokens = [];
        foreach ($this->getObjectListFromDB($sql) as $token) {
            $token = new MRJToken($token);
            $tokens[$token->getX().$token->getY()] = $token;
        }
        return $tokens;
    }

    public function getManholes(){
        return $this->getTokensOfKind(MRJToken::TYPE_MANHOLE);
    }
    
    public function getGaslights(){
        return $this->getTokensOfKind(MRJToken::TYPE_GASLIGHT);
    }
    
    public function getCharacterAt($x, $y){
        $type = MRJToken::TYPE_PAWN;
        $sql = "SELECT type, id, x, y, is_flipped, is_being_moved, rotation FROM token WHERE type='".$type."' AND x = ".$x." AND y = ".$y;
        return new MRJToken($this->getObjectFromDB($sql));        
    }
    
    public function getCharacterBeingMoved(){
        $type = MRJToken::TYPE_PAWN;
        $sql = "SELECT type, id, x, y, is_flipped, is_being_moved, rotation FROM token WHERE type='".$type."' AND is_being_moved = 1";
        return new MRJToken($this->getObjectFromDB($sql));        
    }
    
    public function getWatsonCharacterPawn(){
        $type = MRJToken::TYPE_PAWN;
        $id = MRJCharacter::ID_JOHN_H_WATSON;
        $sql = "SELECT type, id, x, y, is_flipped, is_being_moved, rotation FROM token WHERE type='".$type."' AND id = '".$id."'";
        return new MRJToken($this->getObjectFromDB($sql));        
    }
    
    public function getPoliceCordons(){
        $sql = "SELECT position, is_active, is_being_moved FROM police_cordon ";
        return array_map(function($policeCordon){
            return new MRJPoliceCordon($policeCordon);
        }, $this->getCollectionFromDB($sql));
    }

    public function startMovingPoliceCordon(string $position){
        $sql = "UPDATE police_cordon set is_being_moved = 1 where position = '".$position."'";
        $this->DbQuery($sql);
    }
    
    public function startMovingToken(MRJToken $token){
        $type = $token->getType();
        $id = $token->getId();
        $sql = "UPDATE token set is_being_moved = 1 where type='$type' and id='$id'";
        $this->DbQuery($sql);
    }
    
    public function setTokenRotation(MRJToken $token, string $rotation){
        $type = $token->getType();
        $id = $token->getId();
        $sql = "UPDATE token set rotation = '$rotation' where type='$type' and id='$id'";
        $this->DbQuery($sql);
    }
    
    public function moveCordon(string $position){
        $sql = "UPDATE police_cordon set is_being_moved = 0, is_active = 0 where is_being_moved = 1";
        $this->DbQuery($sql);
        
        $sql = "UPDATE police_cordon set is_active = 1 where position = '".$position."'";
        $this->DbQuery($sql);
    }
    
    public function stopMovingCordon(){
        $sql = "UPDATE police_cordon set is_being_moved = 0 where is_being_moved = 1";
        $this->DbQuery($sql);
    }

    public function setupBoardAndCards() {
        $this->setTokens(MRJBoardFactory::getInitialTokenPositions());
        $this->setPolicecordons(MRJBoardFactory::getInitialPoliceCordons());
        $this->alibiCards->createCards(MRJBoardFactory::getCards(), self::CARD_LOCATION_DECK);
        $this->alibiCards->shuffle(self::CARD_LOCATION_DECK);
        $this->characterCards->createCards(MRJBoardFactory::getCards(), self::CARD_LOCATION_DECK);
        $this->characterCards->shuffle(self::CARD_LOCATION_DECK);
    }

    public function setTokens($token) {
        $sql = "DELETE FROM token";
        $this->DbQuery( $sql );

        $sql ="INSERT INTO token (type, id, x, y, is_flipped, is_being_moved, rotation) VALUES";

        $values = [];
        foreach ($token as $token) {
            $values[] = "('".$token["type"]."', '".$token["id"]."', ".$token["x"].", ".$token["y"].", ".$token["is_flipped"].", 0, '".$token["rotation"]."')";
        }
        $sql .= implode( ',', $values );
        $this->DbQuery( $sql );
    }

    public function moveToken($type, $id, $x, $y, $stopMoving = true){
        $isBeingMoved = $stopMoving ? 0 : 1;
        $sql = "UPDATE token SET x=$x, y=$y, is_being_moved = $isBeingMoved WHERE type = '$type' AND id = '$id'";
        $this->DbQuery( $sql );
    }
    
    public function setCapture($id, $x, $y, $orientation){
        $sql = "UPDATE token SET type='capture', x=$x, y=$y, rotation = '$orientation' WHERE type = 'pawn' AND id = '$id'";
        $this->DbQuery( $sql );
    }

    public function stopMovingToken(){
        $sql = "UPDATE token SET is_being_moved = 0 WHERE is_being_moved = 1";
        $this->DbQuery( $sql );
    }

    public function switchTokens(MRJToken $first, MRJToken $second){
        $this->moveToken($first->getType(), $first->getId(), $second->getX(), $second->getY());
        $this->moveToken($second->getType(), $second->getId(), $first->getX(), $first->getY());
    }
    
    public function setPoliceCordons($policeCordons) {
        $sql = "DELETE FROM police_cordon";
        $this->DbQuery( $sql );

        $sql ="INSERT INTO police_cordon (position, is_active) VALUES";

        $values = [];
        foreach ($policeCordons as $key => $policeCordon) {
            $values[] = "('".$key."', ".$policeCordon["is_active"].")";
        }
        $sql .= implode( ',', $values );
        $this->DbQuery( $sql );
    }

    public function setPawnsToFlipped($characterIds){
        $type = MRJToken::TYPE_PAWN;
        $ids = "'".implode("','", $characterIds)."'";

        $sql = "SELECT id, is_flipped FROM token WHERE type = '$type' AND id in ($ids)";
        $previous = $this->getCollectionFromDb($sql);

        $sql = "UPDATE token SET is_flipped = 1 WHERE type = '$type' AND id in ($ids)";
        $this->DbQuery( $sql );

        return array_filter($characterIds, function($characterId) use ($previous){
            return $previous[$characterId]["is_flipped"] == 0;
        });
    }

    public function turnOffGaslight($id){
        $type = MRJToken::TYPE_GASLIGHT;
        $sql = "DELETE FROM token WHERE type = '$type' AND id = '$id'";
        $this->DbQuery( $sql );
    }

    function setScore($player_id, $count) {
        $this->DbQuery("UPDATE player SET player_score='$count' WHERE player_id='$player_id'");
    }
    
    function canPlayerCancelCharacter(){
        return !$this->checkIfAbilityDone() && !$this->checkIfMoveDone();
    }

    function getLights() {
        $showLights = $this->getGameStateValue(self::OPTION_NO_LIGHTS) != 2;
        return $this->getBoardWithTokens()->getLights($showLights);
    }

    function updatewinStatistics($winningPlayerId, $winningStat){
        $losingPlayerId = $this->getPlayerAfter($winningPlayerId);

        if ($winningStat == self::STAT_WON_BY_CAPTURE){
            $this->setStat(1, self::STAT_DETECTIVE_WON);
            $this->setStat(1, self::STAT_WON_AS_DETECTIVE, $winningPlayerId);
            $this->setStat(1, self::STAT_LOST_AS_MRJACK, $losingPlayerId);    
        } else {
            $this->setStat(1, self::STAT_MRJACK_WON);
            $this->setStat(1, self::STAT_WON_AS_MRJACK, $winningPlayerId);
            $this->setStat(1, self::STAT_LOST_AS_DETECTIVE, $losingPlayerId);    
        }
        
        if ($winningStat == self::STAT_WON_BY_CAPTURE || $winningStat == self::STAT_WON_BY_INCORRECT_CAPTURE){
            $this->setStat(1, self::STAT_END_BY_CAPTURE);
            
            if ($winningStat == self::STAT_WON_BY_CAPTURE){
                $this->setStat(1, self::STAT_CAPTURE_ATTEMPT_SUCCESSFUL);
            }
        } else if ($winningStat == self::STAT_WON_BY_ESCAPE){
            $this->setStat(1, self::STAT_END_BY_ESCAPE);
        } else if ($winningStat == self::STAT_WON_BY_TIME){
            $this->setStat(1, self::STAT_END_BY_TIME);
        }
                
        $this->setStat(1, self::STAT_WON, $winningPlayerId);
        
        $losingStat = $this->getLosingStatFromWinningStat($winningStat);
        $this->setStat(1, $winningStat, $winningPlayerId);
        $this->setStat(1, $losingStat, $losingPlayerId);
    }

    function getLosingStatFromWinningStat($winningStat){
        switch ($winningStat) {
            case self::STAT_WON_BY_CAPTURE:
                return self::STAT_LOST_BY_CAPTURE;
            case self::STAT_WON_BY_ESCAPE:
                return self::STAT_LOST_BY_ESCAPE;
            case self::STAT_WON_BY_INCORRECT_CAPTURE:
                return self::STAT_LOST_BY_INCORRECT_CAPTURE;
            case self::STAT_WON_BY_TIME:
                return self::STAT_LOST_BY_TIME;
            
            default:
                return null;
        }
    }

    function setAllStats($value, $name, $setTableStat = true){
        if ($setTableStat) {
            $this->setStat($value, $name);
        }

        $mrJackPlayerId = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID);
        $otherPlayer = $this->getPlayerAfter($mrJackPlayerId);

        $this->setStat($value, $name, $mrJackPlayerId);
        $this->setStat($value, $name, $otherPlayer);
    }
    
    function incAllStats($delta, $name, $incTableStat = true){
        if ($incTableStat) {
            $this->incStat($delta, $name);
        }

        $mrJackPlayerId = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID);
        $otherPlayer = $this->getPlayerAfter($mrJackPlayerId);

        $this->incStat($delta, $name, $mrJackPlayerId);
        $this->incStat($delta, $name, $otherPlayer);
    }

    function getPickStat($characterId){
        switch ($characterId) {
            case 'sh':
                return self::STAT_PICKED_SH;
            case 'ms':
                return self::STAT_PICKED_MS;
            case 'jhw':
                return self::STAT_PICKED_JHW;
            case 'swg':
                return self::STAT_PICKED_SWG;
            case 'sg':
                return self::STAT_PICKED_SG;
            case 'jb':
                return self::STAT_PICKED_JB;
            case 'js':
                return self::STAT_PICKED_JS;
            case 'il':
                return self::STAT_PICKED_IL;
            
            default:
                return null;
        }
    }

    function updateVisibilityStat($isVisible){
        $round = $this->getGameStateValue(self::GLOBAL_ROUND_NUMBER);
        $percentage = $this->getStat(self::STAT_MRJACK_VISIBLE_PERCENTAGE, $this->getActivePlayerId());

        $numberOfVisibleRounds = round(($round) * $percentage / 100, 2); 
        if ($isVisible){
            $numberOfVisibleRounds++;
        }
        $this->setAllStats(round($numberOfVisibleRounds / ($round + 1) * 100, 2), self::STAT_MRJACK_VISIBLE_PERCENTAGE, false);
    }

    function setInnocenteStat($round, $numberOfInnocents){
        $statName = "";
        $prevStatName = "";

        switch ($round) {
            case 1:
                $statName = self::STAT_INNOCENT_AFTER_1;
                break;
            case 2:
                $statName = self::STAT_INNOCENT_AFTER_2;
                $prevStatName = self::STAT_INNOCENT_AFTER_1;
                break;
            case 3:
                $statName = self::STAT_INNOCENT_AFTER_3;
                $prevStatName = self::STAT_INNOCENT_AFTER_2;
                break;
            case 4:
                $statName = self::STAT_INNOCENT_AFTER_4;
                $prevStatName = self::STAT_INNOCENT_AFTER_3;
                break;
            case 5:
                $statName = self::STAT_INNOCENT_AFTER_5;
                $prevStatName = self::STAT_INNOCENT_AFTER_4;
                break;
            case 6:
                $statName = self::STAT_INNOCENT_AFTER_6;
                $prevStatName = self::STAT_INNOCENT_AFTER_5;
                break;
            case 7:
                $statName = self::STAT_INNOCENT_AFTER_7;
                $prevStatName = self::STAT_INNOCENT_AFTER_6;
                break;
            
            default:
                break;
        }

        $previousNumberOfInnocents = 0;
        if ($prevStatName != ""){
            $previousNumberOfInnocents = $this->getStat($prevStatName, $this->getActivePlayerId());
        }

        $this->initStat("player", $statName, $numberOfInnocents + $previousNumberOfInnocents);
    }

    function getRotationName($rotation){
        $names = [
            "nw" => clienttranslate("northwest"),
            "ne" => clienttranslate("northeast"),
            "sw" => clienttranslate("southwest"),
            "se" => clienttranslate("southeast"),
            "s" => clienttranslate("south"),
            "n" => clienttranslate("north"),
        ];

        if (!array_key_exists($rotation, $names)){
            return "none";
        }

        return $names[$rotation];
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Player actions
//////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in mrjack.action.php)
    */

    public function selectCharacterCard($cardType){
        $this->checkAction( 'selectCharacterCard' );

        $foundCards = $this->characterCards->getCardsOfTypeInLocation($cardType, null, self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_READY);
        
        if (sizeof($foundCards) !== 1){
            throw new BgaUserException( self::_('This card is not in display or is used') );
        }
        $card = reset($foundCards);

        $character = MRJCharacter::getCharacter($cardType);

        $tokens = $this->getTokens();
        $currentToken = $this->array_find($tokens, function($token) use ($card){
            return $token->getId() == $card["type"];
        });
        $isInnocent = $currentToken->getIsFlipped();
        
        $this->characterCards->moveCards(array_keys($foundCards), self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_SELECTED);
        $this->notifyAllPlayers("characterCardSelected", clienttranslate('${player_name} selected ${characterName}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'characterName' => $character->getName(),
            'cardId' => $card["id"],
            'cardType' => $card["type"],
            'isInnocent' => $isInnocent
        ]);


        $this->setGameStateValue(self::GLOBAL_SPECIAL_ACTION_USED, 0);
        $this->setGameStateValue(self::GLOBAL_MOVE_USED, 0);

        $character->postSelectAction($this);
    }

    public function selectCharacterByField($x, $y){
        $characterToken = $this->getCharacterAt($x, $y); 
        $this->selectCharacterCard($characterToken->getId());
    }

    public function decideAbility(){
        //TODO check if possible
        $character = $this->getSelectedCharacter();
        $this->gamestate->nextState($character->getAbilityTransition());
    }
    
    public function cancelCharacter(){
        $this->checkAction( 'cancelCharacter' );
        $state = $this->gamestate->state();
        $isMoveCloserAction = $state["name"] == "moveCloser";

        if (!$this->canPlayerCancelCharacter() && !$isMoveCloserAction){
            throw new BgaUserException( self::_('You cannot cancel character now.') );
        }

        $this->notifyAllPlayers("characterCardCancelled", clienttranslate('${player_name} give up on using selected character'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
        ]);
        
        if ($isMoveCloserAction){
            $characterPawn = $this->getCharacterBeingMoved();
            $this->stopMovingToken($characterPawn->getType(), $characterPawn->getId());
            return $this->gamestate->nextState("movementPointsLeft");
        }

        $this->characterCards->moveAllCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_SELECTED, self::CARD_DISPLAY_STATUS_READY);
        $this->gamestate->nextState("pickCharacter");
    }
    
    public function cancelSelection(){
        $this->checkAction( 'cancelSelection' );
        $state = $this->gamestate->state()["name"];

        if ($state == "selectCordonDestination"){
            $this->stopMovingCordon();
        } else {
            $this->stopMovingToken();
        }

        $this->gamestate->nextState("cancelSelection");
    }
    
    public function cancelAbility(){
        $this->checkAction( 'cancelAbility' );

        $state = $this->gamestate->state();
        $isMoveCloserAction = $state["name"] == "pickCharacterToMoveCloser";

        if ($isMoveCloserAction){
            $movePointsLeft = $this->getGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT);
            if ($movePointsLeft < 3){
                throw new BgaUserException( self::_('You cannot cancel ability use as you already moved characters.') );
            }
        }

        if (!$this->canPlayerCancelCharacter()){
            throw new BgaUserException( self::_('You cannot cancel ability use now.') );
        }

        $this->gamestate->nextState("cancel");
    }

    public function moveCharacter($x, $y){
        $this->checkAction( 'moveCharacter' );
        $selectedCharacter = $this->getSelectedCharacter();

        $possibleMoves = $this->getPossibleMoves($selectedCharacter)["fields"];

        if (!array_key_exists($x.$y, $possibleMoves)){
            throw new BgaUserException( self::_('This cooridinates are not valid') );
        }

        $this->moveToken(MRJToken::TYPE_PAWN, $selectedCharacter->getId(), $x, $y);
        $this->setGameStateValue(self::GLOBAL_MOVE_USED, 1);

        $target = $possibleMoves[$x.$y];
        
        $this->notifyAllPlayers("tokenMoved", clienttranslate('${player_name} moved ${characterName}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'id' => $selectedCharacter->getId(),
            'characterName' => $selectedCharacter->getName(),
            'type' => MRJToken::TYPE_PAWN,
            'lights' => $this->getLights(),
            'target' => $target
        ]);

        if ($selectedCharacter->getId() == MRJCharacter::ID_SIR_WILLIAM_GULL){
            $this->incStat(1, self::STAT_SWG_MOVED, $this->getActivePlayerId());
        }

        $selectedCharacter->postMoveAction($this);
    }

    function characterCapture($x, $y){
        $this->checkAction( 'characterCapture' );
        $selectedCharacter = $this->getSelectedCharacter();

        $possibleCaptures = $this->getPossibleMoves($selectedCharacter)["captures"];
        if (!array_key_exists($x.$y, $possibleCaptures)){
            throw new BgaUserException( self::_('This cooridinates are not valid') );
        }

        $this->incStat(1, $this->getPickStat($selectedCharacter->getId()), $this->getActivePlayerId());
        if ($selectedCharacter->getId() == MRJCharacter::ID_SIR_WILLIAM_GULL){
            $this->incStat(1, self::STAT_SWG_MOVED, $this->getActivePlayerId());
        }

        $target = $possibleCaptures[$x.$y];
        $targetCharacter = $this->getCharacterAt($x, $y);

        $this->setCapture($selectedCharacter->getId(), $x, $y, $target["from"]);
        $this->characterCards->moveAllCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_SELECTED, self::CARD_DISPLAY_STATUS_USED);
        
        $this->notifyAllPlayers("tokenCaptured", clienttranslate('${player_name} captures ${capturedCharacter} with ${characterName}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'id' => $selectedCharacter->getId(),
            'characterName' => $selectedCharacter->getName(),
            'capturedCharacter' => MRJCharacter::getCharacter($targetCharacter->getId())->getName(),
            'type' => MRJToken::TYPE_PAWN,
            'target' => $target
        ]);

        $pawn = $this->getCharacterAt($x, $y);

        $isCaptureSuccess = $this->getMrJackCharacter() == $pawn->getId();
        
        $winningPlayerId = $this->getCurrentPlayerId();
        if (!$isCaptureSuccess){
            $winningPlayerId = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID);
        }

        $this->setGameStateValue(self::GLOBAL_MR_JACK_KNOWN, 1);

        $message = $isCaptureSuccess ? self::_("Mr. Jack is captured!") : self::_("Wrong character is accused, so Mr. Jack is free!");
        $this->notifyAllPlayers("gameEnd", $message, [
            "x" => $x,
            "y" => $y
        ]);
        
        $this->notifyAllPlayers("gameEndDialog", "", [
            "message" => $message,
            "mrJackCharacterId" => $this->getMrJackCharacter()
        ]);

        $this->setScore($winningPlayerId, 1);
        $this->notifyAllPlayers("setScore", "", [
            "playerId" => $winningPlayerId,
            "score" => 1
        ]);

        $this->updateWinStatistics($winningPlayerId, $isCaptureSuccess ? self::STAT_WON_BY_CAPTURE : self::STAT_WON_BY_INCORRECT_CAPTURE);

        $this->gamestate->nextState("endGame");
    }

    function characterExit($x, $y){
        $this->checkAction( 'characterExit' );
        $selectedCharacter = $this->getSelectedCharacter();

        $possibleExits = $this->getPossibleMoves($selectedCharacter)["exits"];
        if (!array_key_exists($x.$y, $possibleExits)){
            throw new BgaUserException( self::_('This cooridinates are not valid') );
        }

        $this->moveToken(MRJToken::TYPE_PAWN, $selectedCharacter->getId(), $x, $y);

        $this->incStat(1, $this->getPickStat($selectedCharacter->getId()), $this->getActivePlayerId());
        if ($selectedCharacter->getId() == MRJCharacter::ID_SIR_WILLIAM_GULL){
            $this->incStat(1, self::STAT_SWG_MOVED, $this->getActivePlayerId());
        }

        $target = $possibleExits[$x.$y];
        
        $this->notifyAllPlayers("tokenMoved", clienttranslate('${player_name} escapes with ${characterName} to ${exitName}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'id' => $selectedCharacter->getId(),
            'characterName' => $selectedCharacter->getName(),
            'type' => MRJToken::TYPE_PAWN,
            'target' => $target,
            'exitName' => $this->getRotationName($target["field"]->getExitTo())
        ]);

        $this->setGameStateValue(self::GLOBAL_MR_JACK_KNOWN, 1);
        $this->notifyAllPlayers("gameEnd", clienttranslate("Mr. Jack escaped"), [
            "x" => $x,
            "y" => $y
        ]);
        $this->notifyAllPlayers("gameEndDialog", "", [
            "message" => self::_("Mr. Jack escaped!"),
            "mrJackCharacterId" => $this->getMrJackCharacter()
        ]);

        $mrJackPlayerId = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID);
        $this->setScore($mrJackPlayerId, 1);
        $this->notifyAllPlayers("setScore", "", [
            "playerId" => $mrJackPlayerId,
            "score" => 1
            ]);
        
        $this->updateWinStatistics($mrJackPlayerId, self::STAT_WON_BY_ESCAPE);
        $this->gamestate->nextState("endGame");
    }

    function selectSourceCordon(string $position){
        $this->checkAction( 'selectSourceCordon' );

        $cordons = $this->getPoliceCordons();
        $selectedCordon = $cordons[$position];

        if (!$selectedCordon->getIsActive()){
            throw new BgaUserException( self::_('This cordon is not active.') );
        }

        $this->startMovingPoliceCordon($position);

        $this->gamestate->nextState("selected");
    }
    
    function selectCordonDestination(string $position){
        $this->checkAction( 'selectCordonDestination' );

        $cordons = $this->getPoliceCordons();
        $selectedCordon = $cordons[$position];

        if ($selectedCordon->getIsActive()){
            throw new BgaUserException( self::_('This cordon is already active.') );
        }

        $this->moveCordon($position);

        $sourceCordon = $this->array_find($cordons, function($cordon){
            return $cordon->getIsBeingMoved();
        });

        $this->notifyAllPlayers("cordonMoved", clienttranslate('${player_name} moved police cordon from ${fromName} to ${toName}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            "from" => $sourceCordon->getPosition(),
            "to" =>  $position,
            "fromName" => $this->getRotationName($sourceCordon->getPosition()),
            "toName" =>  $this->getRotationName($position)
        ]);

        $this->gamestate->nextState("afterSpecialAction");
    }
    
    function selectSourceManhole(int $x, int $y){
        $this->checkAction( 'selectSourceManhole' );

        $manholes = $this->getManholes();
        
        if (!array_key_exists($x.$y, $manholes)){
            throw new BgaUserException( self::_("This field doesn't contain manhole token.") );
        }

        $this->startMovingToken($manholes[$x.$y]);

        $this->gamestate->nextState("selected");
    }
    
    function selectManholeDestination(int $x, int $y){
        $this->checkAction( 'selectManholeDestination' );

        $manholes = $this->getManholes();
        $fields = $this->getBoardFields();

        if (!array_key_exists($x.$y, $fields) || !$fields[$x.$y]->getIsManhole()){
            throw new BgaUserException( self::_("This field cannot contain manhole token.") );
        }
        
        if (array_key_exists($x.$y, $manholes)){
            throw new BgaUserException( self::_("This field already contains manhole token.") );
        }

        $source = $this->array_find($manholes, function($manhole){
            return $manhole->getIsBeingMoved();
        });

        $this->moveToken($source->getType(), $source->getId(), $x, $y);

        $this->notifyAllPlayers("tokenMoved", clienttranslate('${player_name} moved manhole'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'id' => $source->getId(),
            'type' => MRJToken::TYPE_MANHOLE,
            'target' => [
                "field" => $fields[$x.$y]
            ]
        ]);

        $this->gamestate->nextState("afterSpecialAction");
    }
    
    function selectSourceGaslight(int $x, int $y){
        $this->checkAction( 'selectSourceGaslight' );

        $gaslights = $this->getGaslights();
        
        if (!array_key_exists($x.$y, $gaslights)){
            throw new BgaUserException( self::_("This field doesn't contain gaslight token.") );
        }

        $this->startMovingToken($gaslights[$x.$y]);

        $this->gamestate->nextState("selected");
    }
    
    function selectGaslightDestination(int $x, int $y){
        $this->checkAction( 'selectGaslightDestination' );

        $gaslights = $this->getGaslights();
        $fields = $this->getBoardFields();

        if (!array_key_exists($x.$y, $fields) || !$fields[$x.$y]->getIsGaslight()){
            throw new BgaUserException( self::_("This field cannot contain gaslight token.") );
        }
        
        if (array_key_exists($x.$y, $gaslights)){
            throw new BgaUserException( self::_("This field already contains gaslight token.") );
        }

        $source = $this->array_find($gaslights, function($gaslight){
            return $gaslight->getIsBeingMoved();
        });

        $this->moveToken($source->getType(), $source->getId(), $x, $y);

        $this->notifyAllPlayers("tokenMoved", clienttranslate('${player_name} moved gaslight'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'id' => $source->getId(),
            'type' => MRJToken::TYPE_GASLIGHT,
            'lights' => $this->getLights(),
            'target' => [
                "field" => $fields[$x.$y]
            ]
        ]);

        $this->gamestate->nextState("afterSpecialAction");
    }

    public function switchPlace($x, $y){
        $this->checkAction( 'switchPlace' );

        $selectedCharacter = $this->getSelectedCharacter();
        $pawns = $this->getBoardWithTokens()->getPawns();
        
        if (!array_key_exists($x.$y, $pawns)){
            throw new BgaUserException( self::_("There is no character on this field") );
        }

        $target = $pawns[$x.$y];

        if ($target->getId() == $selectedCharacter->getId()){
            throw new BgaUserException( self::_("You cannot switch with itself") );
        }

        $source = $this->array_find($pawns, function($pawn) use ($selectedCharacter){
            return $pawn->getId() == $selectedCharacter->getId();
        });

        $this->switchTokens($target, $source);

        $this->notifyAllPlayers("switchTokens", clienttranslate('${player_name} switched ${firstCharacter} and ${secondCharacter}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            "first" => $source,
            "second" => $target,
            "firstCharacter" => $selectedCharacter->getName(),
            "secondCharacter" => MRJCharacter::getCharacter($target->getId())->getName(),
            "lights" => $this->getLights()
        ]);

        $this->incStat(1, self::STAT_SWG_SWITCH, $this->getActivePlayerId());

        $isWatsonCharacter = $target->getId() == MRJCharacter::ID_JOHN_H_WATSON;

        if ($isWatsonCharacter){
            return $this->gamestate->nextState("rotateWatson");
        }

        $this->gamestate->nextState("endTurn");
    }
    
    public function pickCharacterToMoveCloser($x, $y){
        $this->checkAction( 'pickCharacterToMoveCloser' );

        $selectedCharacter = $this->getSelectedCharacter();
        $pawns = $this->getBoardWithTokens()->getPawns();
        
        if (!array_key_exists($x.$y, $pawns)){
            throw new BgaUserException( self::_("There is no character on this field") );
        }

        $target = $pawns[$x.$y];

        if ($target->getId() == $selectedCharacter->getId()){
            throw new BgaUserException( self::_("You cannot select itself") );
        }

        $this->startMovingToken($target);

        $this->gamestate->nextState("moveCloser");
    }
    
    public function moveCloser($x, $y){
        $this->checkAction( 'moveCloser' );

        $characterPawn = $this->getCharacterBeingMoved();
        $selectedCharacter = $this->getSelectedCharacter();
        $isWatsonCharacter = $characterPawn->getId() == MRJCharacter::ID_JOHN_H_WATSON;

        $movePointsLeft = $this->getGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT);
        $distances = $this->getBoardWithTokens()->getCharacterMovesTowardsAnother($characterPawn, $selectedCharacter, $movePointsLeft);

        if (!array_key_exists($x.$y, $distances)){
            throw new BgaUserException( self::_("This field is not legal") );
        }

        $distance = $distances[$x.$y]["distance"];
        $this->moveToken($characterPawn->getType(), $characterPawn->getId(), $x, $y, !$isWatsonCharacter);

        $this->notifyAllPlayers("tokenMoved", clienttranslate('${player_name} moved ${characterName} ${count} step(s) closer to ${sergeantGoodleyName}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'id' => $characterPawn->getId(),
            'characterName' => MRJCharacter::getCharacter($characterPawn->getId())->getName(),
            'type' => $characterPawn->getType(),
            'lights' => $this->getLights(),
            'count' => $distance,
            'target' => $distances[$x.$y],
            'sergeantGoodleyName' => MRJSergeantGoodley::getName()
        ]);
        
        $movePointsLeft -= $distance;

        $this->setGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT, $movePointsLeft);
        
        if ($isWatsonCharacter){
            return $this->gamestate->nextState("rotateWatson");
        } 
        
        if ($movePointsLeft > 0){
            $this->gamestate->nextState("movementPointsLeft");
        } else {
            $this->gamestate->nextState("afterSpecialAction");
        }
    }

    public function rotateWatson($x, $y){
        $this->checkAction( 'rotateWatson' );

        $selectedCharacter = $this->getSelectedCharacter();
        $isWatsonActiveCharacter = $selectedCharacter->getId() == MRJCharacter::ID_JOHN_H_WATSON;

        if (!$isWatsonActiveCharacter){
            $selectedCharacter = $this->getWatsonCharacterPawn();
        }

        $board = $this->getBoardWithTokens();
        $selectedCharacterField = $board->getCharacterField($selectedCharacter->getId());
        $characterPawn = $board->getCharacterToken($selectedCharacter->getId());

        $neighboringFieldCoordinates = $selectedCharacterField->getAdjacentFieldCoordinates();

        if (array_search($x.$y, $neighboringFieldCoordinates) === false ){
            throw new BgaUserException( self::_("This field is not legal") );
        }

        $rotation = $selectedCharacterField->getRotationFromAdjacentCoordinates($x, $y);
        $this->setTokenRotation($characterPawn, $rotation);

        $this->notifyAllPlayers("rotateWatson", clienttranslate('${player_name} rotated ${watsonName} towards ${rotation}'), [
            'player_id' => $this->getCurrentPlayerId(),
            'player_name' => $this->getActivePlayerName(),
            'lights' => $this->getLights(),
            'watsonName' => MRJJohnHWatson::getName(),
            'rotation' => $this->getRotationName($rotation)
        ]);
    }

    function confirmWatsonRotation()
    {
        $this->checkAction('confirmWatsonRotation');
        $selectedCharacter = $this->getSelectedCharacter();
        $isWatsonActiveCharacter = $selectedCharacter->getId() == MRJCharacter::ID_JOHN_H_WATSON;
        $isGullActiveCharacter = $selectedCharacter->getId() == MRJCharacter::ID_SIR_WILLIAM_GULL;

        if ($isWatsonActiveCharacter || $isGullActiveCharacter){
            $this->gamestate->nextState("endTurn");
        } else {
            $selectedCharacter = $this->getCharacterBeingMoved();
            $movePointsLeft = $this->getGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT);
            $this->stopMovingToken($selectedCharacter->getType(), $selectedCharacter->getId());
            if ($movePointsLeft > 0){
                $this->gamestate->nextState("movementPointsLeft");
            } else {
                $this->gamestate->nextState("afterSpecialAction");
            }
        }
    }

    /*

    
    Example:

    function playCard( $card_id )
    {
        // Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        $this->checkAction( 'playCard' ); 
        
        $player_id = $this->getActivePlayerId();
        
        // Add your game logic to play a card there 
        ...
        
        // Notify all players about the card played
        $this->notifyAllPlayers( "cardPlayed", clienttranslate( '${player_name} plays ${card_name}' ), array(
            'player_id' => $player_id,
            'player_name' => $this->getActivePlayerName(),
            'card_name' => $card_name,
            'card_id' => $card_id
        ) );
          
    }
    
    */

    
//////////////////////////////////////////////////////////////////////////////
//////////// Game state arguments
////////////

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */
    function argPickCharacterCard(){
        $board = $this->getBoardWithTokens();
        $availableCards = $this->characterCards->getCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_READY);
        $cardCount = sizeof($availableCards);
        $characterFields = array_map(function($card) use ($board) {
            return $board->getCharacterField($card["type"]);
        }, $availableCards);

        $characterText = self::_("one character");

        if ($cardCount == 3 || $cardCount == 2){
            $characterText = $cardCount == 2 ? self::_("second out of two characters") : self::_("first out of two characters"); 
        }

        return [
            "fields" => $characterFields,
            "characterText" => $characterText
        ];
    }

    function argMoveCharacter(){
        $selectedCharacter = $this->getSelectedCharacter();
        $possibleMoves = $selectedCharacter === null ? [] : $this->getPossibleMoves($selectedCharacter);
        
        $selectedCharacterField = $selectedCharacter === null ? null : $this->getBoardWithTokens()->getCharacterField($selectedCharacter->getId());
        return [
            "_private" => [
                "active" => [
                    "possibleMoves" => $possibleMoves,
                    ]
            ],
            "usedSpecialAbility" => $this->getGameStateValue(self::GLOBAL_SPECIAL_ACTION_USED) == 1,
            "canCancel" => $this->canPlayerCancelCharacter(),
            "characterName" => $selectedCharacter->getName(),
            "selectedCharacterField" => $selectedCharacterField
        ];
    }

    function argMoveCordon(){
        return [
            "cordons" => $this->getPoliceCordons(),
            "canCancel" => $this->canPlayerCancelCharacter()
        ];
    }

    function argSelectSourceManhole(){
        $manholes = $this->getManholes();
        return [
            "manholes" => $manholes,
            "canCancel" => $this->canPlayerCancelCharacter()
        ];
    }
    
    function argSelectManholeDestination(){
        $manholes = $this->getManholes();
        $fields = array_filter($this->getBoardFields(), function($field) use ($manholes){
            if ($field->getIsManhole()){
                return !array_key_exists($field->getCoordinatesKey(), $manholes);
            }

            return false;
        });
        return [
            "fields" => $fields
        ];
    }
    
    function argSelectSourceGaslight(){
        $gaslights = $this->getGaslights();
        return [
            "gaslights" => $gaslights,
            "canCancel" => $this->canPlayerCancelCharacter()
        ];
    }
    
    function argSelectGaslightDestination(){
        $gaslights = $this->getGaslights();
        $fields = array_filter($this->getBoardFields(), function($field) use ($gaslights){
            if ($field->getIsGaslight()){
                return !array_key_exists($field->getCoordinatesKey(), $gaslights);
            }

            return false;
        });
        return [
            "fields" => $fields
        ];
    }

    function argOtherCharacters() {
        $selectedCharacter = $this->getSelectedCharacter();
        $otherCharacters =  array_filter($this->getBoardWithTokens()->getPawns(), function($pawn) use($selectedCharacter){
            return $pawn->getId() != $selectedCharacter->getId();
        });
        return [
            "characters" => json_encode($otherCharacters),
            "canCancel" => $this->canPlayerCancelCharacter()
        ];
    }

    function argPickCharacterToMoveCloser() {
        $selectedCharacter = $this->getSelectedCharacter();
        $otherCharacters = [];
        
        $board = $this->getBoardWithTokens();
        
        $count = $this->getGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT);
        
        $otherCharacters =  array_filter($board->getPawns(), function($pawn) use($selectedCharacter, $board, $count){
            $characterId = $pawn->getId();
            $character = MRJCharacter::getCharacter($characterId);
            $distances = $board->getCharacterMovesTowardsAnother($character, $selectedCharacter, $count);
            return sizeof($distances) > 0;
        });

        return [
            "characters" => json_encode($otherCharacters),
            "count" => $count,
            "canCancel" => $this->canPlayerCancelCharacter() && $count == 3
        ];
    }
    
    function argMoveCloser() {
        $characterPawn = $this->getCharacterBeingMoved();
        $selectedCharacter = $this->getSelectedCharacter();
        $movePointsLeft = $this->getGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT);
        $distances = json_encode($this->getBoardWithTokens()->getCharacterMovesTowardsAnother($characterPawn, $selectedCharacter, $movePointsLeft));
        return [
            "type" => $characterPawn->getId(),
            "characterName" => MRJCharacter::getCharacter($characterPawn->getId())->getName(),
            "distances" => $distances,
            "count" => $movePointsLeft
        ];
    }

    function argRotateWatson() {
        $selectedCharacter = $this->getSelectedCharacter();

        if ($selectedCharacter->getId() != MRJCharacter::ID_JOHN_H_WATSON){
            $selectedCharacter = $this->getWatsonCharacterPawn();
        }

        $board = $this->getBoardWithTokens();
        $selectedCharacterField = $board->getCharacterField($selectedCharacter->getId());

        $neighboringFieldCoordinates = $selectedCharacterField->getAdjacentFieldCoordinates();
        
        $neighboringFields = array_filter($board->getFields(), function($field) use ($neighboringFieldCoordinates){
            return array_search($field->getCoordinatesKey(), $neighboringFieldCoordinates) !== false;
        });

        $neighboringFields = array_map(function($field) use ($selectedCharacterField){
            return [
                "field" => $field,
                "direction" => $selectedCharacterField->getRotationFromAdjacentCoordinates($field->getX(), $field->getY())
            ];
        }, $neighboringFields);

        return [
            "fields" => json_encode($neighboringFields)
        ];
    }

    /*
    
    Example for game state "MyGameState":
    
    function argMyGameState()
    {
        // Get some values from the current game situation in database...
    
        // return values:
        return array(
            'variable1' => $value1,
            'variable2' => $value2,
            ...
        );
    }    
    */

//////////////////////////////////////////////////////////////////////////////
//////////// Game state actions
////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */
    
    function stRoundSetup(){
        $round = $this->incGameStateValue(self::GLOBAL_ROUND_NUMBER, 1);
        $this->incAllStats(1, self::STAT_ROUND_NUMBER);
        
        $this->characterCards->pickCardsForLocation(4, self::CARD_LOCATION_DECK, self::CARD_LOCATION_DISPLAY, 0);

        $this->notifyAllPlayers("newRound", clienttranslate('Round ${round} starts. 4 new character cards are dealt to display'), [
            "round" => $round,
            "characterCards" => $this->getDisplayCardsWithCharacterStatuses(),
            "remainingCharacterCardsNumber" => $this->characterCards->countCardInLocation(self::CARD_LOCATION_DECK)
        ]);

        $activePlayerId = $this->getActivePlayerId();
        $this->giveExtraTime($activePlayerId);
        $this->giveExtraTime($this->getPlayerAfter($activePlayerId));

        $this->gamestate->nextState("pickCharacter");
    }

    public function stTurnEnd()
    {        
        $foundCards = $this->characterCards->getCardsInLocation(self::CARD_LOCATION_DISPLAY, 1);

        $this->characterCards->moveAllCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_SELECTED, self::CARD_DISPLAY_STATUS_USED);

        $characterId = reset($foundCards)["type"];
        $this->notifyAllPlayers("cardDiscarded", "", [
            "type" => $characterId
        ]);

        $this->incStat(1, $this->getPickStat($characterId), $this->getActivePlayerId());

        $readyCardsInDisplay = $this->characterCards->getCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_DISPLAY_STATUS_READY);

        if (sizeof($readyCardsInDisplay) === 3 || sizeof($readyCardsInDisplay) === 1){
            $this->activeNextPlayer();
        }
        
        if (sizeof($readyCardsInDisplay) === 0){
            $this->activeNextPlayer();
            return $this->gamestate->nextState("newRound");
        }
        
        $this->gamestate->nextState("pickCharacterCard");
    }

    function stRoundWrapUp(){
        //determine character visibilities
        $visibilities = $this->getCharacterVisibilities();

        //get mr. jack character
        $mrJackCharacter = $this->getMrJackCharacter();

        $isMrJackVisible = array_key_exists($mrJackCharacter ,$visibilities["visible"]);

        $message = $isMrJackVisible ? clienttranslate('Mr. Jack is visible!') : clienttranslate('Mr. Jack is not visible');
        $this->setGameStateValue(self::GLOBAL_MR_JACK_WAS_VISIBLE, $isMrJackVisible ? 1 : 0);

        $this->updateVisibilityStat($isMrJackVisible);
        
        $this->notifyAllPlayers("mrJackVisibility", $message, [
            "isMrJackVisible" => $isMrJackVisible
        ]);

        //flip innocent characters
        $innocentCharacters = $isMrJackVisible ? $visibilities["invisible"] : $visibilities["visible"];
        $innocents = $this->setPawnsToFlipped(array_keys($innocentCharacters));

        $innocents = array_map(function($id){
            return MRJCharacter::getCharacter($id)->getName();
        }, $innocents);

        $message = clienttranslate('New characters are proven innocent: ${characterNames}');
        if (sizeof($innocents) == 0){
            $message = clienttranslate('No new characters are proven innocent.');
        }
        
        $this->notifyAllPlayers("updateCharacterInnocence", $message, [
            "tokens" => json_encode($this->getTokens()),
            "characterNames" => implode(", ", $innocents)
        ]);

        $round = $this->getGameStateValue(self::GLOBAL_ROUND_NUMBER);
        if ($round < 5){
            $this->turnOffGaslight($round);

            $this->notifyAllPlayers("gaslightTurnOff", clienttranslate('Gaslight ${id} turned off'), [
                "id" => $round,
                "lights" => $this->getLights()
            ]);
        }

        if ($round == 8){
            $mrJackPlayerId = $this->getGameStateValue(self::GLOBAL_MR_JACK_PLAYER_ID);

            $pawn = $this->getBoardWithTokens()->getCharacterField($mrJackCharacter);
            
            $this->setGameStateValue(self::GLOBAL_MR_JACK_KNOWN, 1);

            $message = self::_("No time left! Mr. Jack escapes!");
            $this->notifyAllPlayers("gameEnd", $message, [
                "x" => $pawn->getX(),
                "y" => $pawn->getY()
            ]);
            
            $this->notifyAllPlayers("gameEndDialog", "", [
                "message" => $message,
                "mrJackCharacterId" => $this->getMrJackCharacter()
            ]);

            $this->setScore($mrJackPlayerId, 1);
            $this->notifyAllPlayers("setScore", "", [
                "playerId" => $mrJackPlayerId,
                "score" => 1
            ]);
            
            $this->updateWinStatistics($mrJackPlayerId, self::STAT_WON_BY_TIME);
            return $this->gamestate->nextState("endGame");
        }

        $this->setInnocenteStat($round, sizeof($innocents));

        //discard cards from display
        $this->characterCards->moveAllCardsInLocation(self::CARD_LOCATION_DISPLAY, self::CARD_LOCATION_DISCARD, self::CARD_DISPLAY_STATUS_USED);

        $this->gamestate->nextState("nextRound");
    }

    function stDealAlibiCard(){
        $playerId = $this->getActivePlayerId();
        
        $card = $this->alibiCards->pickCardForLocation( self::CARD_LOCATION_DECK, self::CARD_LOCATION_HAND, $playerId );

        $this->notifyAllPlayers("dealAlibiCard", clienttranslate('${player_name} received an alibi card'), [
            'player_id' => $playerId,
            'player_name' => $this->getActivePlayerName(),
            'newAlibiCardsCount' => $this->alibiCards->countCardInLocation(self::CARD_LOCATION_DECK)
        ]);

        $this->notifyPlayer($playerId, "dealAlibiCardPrivate", clienttranslate('You received ${characterName}'), [
            "type" => $card["type"],
            "characterName" => MRJCharacter::getCharacter($card["type"])->getName()
        ]);

        $this->gamestate->nextState("afterSpecialAction");
    }

    function stAfterSpecialAction(){
        $this->setGameStateValue(self::GLOBAL_SPECIAL_ACTION_USED, 1);

        if ($this->getGameStateValue(self::GLOBAL_MOVE_USED) == 0){
            return $this->gamestate->nextState("moveCharacter");
        }

        $this->gamestate->nextState("endTurn");
    }

    function stStartMovecloser(){
        $this->setGameStateValue(self::GLOBAL_SG_MOVEMENT_POINTS_LEFT, 3);
        $this->gamestate->nextState("pickCharacterTomoveCloser");
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Zombie
////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
        
        Important: your zombie code will be called when the player leaves the game. This action is triggered
        from the main site and propagated to the gameserver from a server, not from a browser.
        As a consequence, there is no current player associated to this action. In your zombieTurn function,
        you must _never_ use getCurrentPlayerId() or getCurrentPlayerName(), otherwise it will fail with a "Not logged" error message. 
    */

    function zombieTurn( $state, $active_player )
    {
    	$statename = $state['name'];
    	
        if ($state['type'] === "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState( "zombiePass" );
                	break;
            }

            return;
        }

        if ($state['type'] === "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $this->gamestate->setPlayerNonMultiactive( $active_player, '' );
            
            return;
        }

        throw new feException( "Zombie mode not supported at this game state: ".$statename );
    }
    
///////////////////////////////////////////////////////////////////////////////////:
////////// DB upgrade
//////////

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been published on BGA.
        Once your game is on BGA, this method is called everytime the system detects a game running with your old
        Database scheme.
        In this case, if you change your Database scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run with your new version.
    
    */
    
    function upgradeTableDb( $from_version )
    {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345
        
        // Example:
//        if( $from_version <= 1404301345 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "ALTER TABLE DBPREFIX_xxxxxxx ....";
//            $this->applyDbUpgradeToAllDB( $sql );
//        }
//        if( $from_version <= 1405061421 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "CREATE TABLE DBPREFIX_xxxxxxx ....";
//            $this->applyDbUpgradeToAllDB( $sql );
//        }
//        // Please add your future database scheme changes here
//
//


    }    
}
